<?php
function database()
{
    include_once('config.php');

    $app_db_connection = new PDO(
        'mysql:host=' . config::host . ';dbname=' . config::dbName . ';charset=utf8mb4',
        Config::username,
        Config::password
    );
    return $app_db_connection;
}
?>