#Music Site
##My CA3 


so this site is very simple and it is made to keep track of the albums bands have released or at least the ones that your interested in.
There are three the home page, the bands page and the albums page. There was originally meant to be more pages and a way to add songs to tdiffrent albums but with time constants and database structure issues this proved difficult.



#Config as a Class?  
![alt text](https://i.imgur.com/MGOwvAp.png)


i tried to get the config.php to work as an array but i found it easier to work with it as a class. This is just my personal preference not sure if there is any security risk in doing this  so that is something I'll have to look into.


#Database connection as a function 

![alt text](https://i.imgur.com/5GTakUk.png)

This is pretty much the same reason i made the config a class i found it easier to just return the database connection through a function it made it easier to call it when need insted of defining it each time

#How My WebPage Works

![alt text](https://i.imgur.com/Vao4BZK.png)

Its _VERY_  simple on both form pages you are meet with 3 separate forms one for adding one for removing and one for updating 
either bands or albums. fill in the required fields and watch the table. the pages also give you a success message. 
