<?php
return function($req, $res) {

    $app_db_connection = database();

    $select = "select * from albums";
    $selectStatement = $app_db_connection ->prepare($select);
    $selectStatement->execute();
    $count = $selectStatement->rowCount();

    $albumName= $req->body('albumName');
    $bandName= $req->body('bandName');

    $add = "INSERT INTO albums VALUES (:album_name, :band_name)";
    $insert = $app_db_connection -> prepare($add);
    $insert -> execute([
        'album_name'=>$albumName,
        ':band_name' => $bandName,
    ]);


    $select = "select * from albums";
    $selectStatement = $app_db_connection ->prepare($select);
    $selectStatement->execute();
    $newCount = $selectStatement->rowCount();

    if ($newCount == $count+1)
    {
        $res->redirect("/add-album?success=1");
    }
    else
    {
        $res->redirect("/add-album?success=0");
    }
}
?>