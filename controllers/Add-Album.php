<?php return function($req, $res) {
    $removed =$req->query('removed');
    $added =$req->query('success');
    $updated =$req->query('update');

    $res->render('main','addAlbum', [
        'pageTitle'=>'Add Album',
        'removed'=> $removed,
        'added'=>$added,
        'updated'=>$updated
    ]);
}
?>