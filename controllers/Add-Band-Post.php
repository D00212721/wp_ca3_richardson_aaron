<?php
return function($req, $res) {

    $app_db_connection = database();

    $select = "select * from bands";
    $selectStatement = $app_db_connection ->prepare($select);
    $selectStatement->execute();
    $count = $selectStatement->rowCount();

    $bandName= $req->body('bandName');
    $bandYear= $req->body('bandYear');

    $add = "INSERT INTO bands VALUES (:band_name, :band_year)";
    $insert = $app_db_connection -> prepare($add);
    $insert -> execute([
        ':band_name' => $bandName,
        ':band_year' => $bandYear,

    ]);


    $select = "select * from bands";
    $selectStatement = $app_db_connection ->prepare($select);
    $selectStatement->execute();
    $newCount = $selectStatement->rowCount();

    if ($newCount == $count+1)
    {
        $res->redirect("/add-band?success=1");
    }
    else
        {
            $res->redirect("/add-band?success=0");
        }
}
?>