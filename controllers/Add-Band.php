<?php return function($req, $res) {

    $removed =$req->query('removed');
    $added =$req->query('success');
    $updated =$req->query('update');

    $res->render('main','addBand',
        [
            'pageTitle'=>'Add Band',
            'removed'=> $removed,
            'added'=>$added,
            'updated'=>$updated
        ]
    );
}
?>