<?php return function($req, $res) {

    $app_db_connection = database();


    $select = "select * from albums";
    $selectStatement = $app_db_connection ->prepare($select);
    $selectStatement->execute();
    $count = $selectStatement->rowCount();


    $albumName= $req->body('albumName');
    $bandName= $req->body('bandName');

    $remove_albums = "Delete From albums where band_name = :band_name and album_name = :album_name";
    $remove_album_stt = $app_db_connection->prepare($remove_albums);
    $remove_album_stt->execute([
    ':band_name'=>$bandName,
    ':album_name'=>$albumName,
]);


    $select = "select * from albums";
    $selectStatement = $app_db_connection ->prepare($select);
    $selectStatement->execute();
    $newCount = $selectStatement->rowCount();



    if ($newCount == $count-1)
    {
        $res->redirect('/add-album?removed=1');
    }
    else
    {
        $res->redirect("/add-album?removed=0");
    }


};