<?php

return function($req, $res) {



    $app_db_connection = database();


    $select = "select * from bands";
    $selectStatement = $app_db_connection ->prepare($select);
    $selectStatement->execute();
    $count = $selectStatement->rowCount();


    $bandName= $req->body('bandName');
    $bandYear= $req->body('bandYear');

    $remove_albums = "Delete From albums where band_name = :band_name";
    $remove_album_stt = $app_db_connection->prepare($remove_albums);
    $remove_album_stt->execute([
        ':band_name'=>$bandName,
    ]);

    $remove_band = "delete from bands where band_name = :band_name and band_year = :band_year";
    $remove_band_stt = $app_db_connection -> prepare($remove_band);
    $remove_band_stt -> execute([
        ':band_name' => $bandName,
        ':band_year' => $bandYear,

    ]);


    $selectStatement->execute();
    $newCount = $selectStatement->rowCount();

    if ($newCount == $count-1)
    {
        $res->redirect('/add-band?removed=1');
    }
    else
    {
        $res->redirect("/add-band?removed=0");
    }


}
?>