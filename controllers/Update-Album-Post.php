<?php

return function($req, $res) {


    $app_db_connection = database();


    $select = "select * from albums";
    $selectStatement = $app_db_connection->prepare($select);
    $selectStatement->execute();


    $albumName = $req->body('oldAlbumName');
    $bandName = $req->body('oldBandName');
    $newAlbumName = $req->body('newAlbumName');
    $newBandName = $req->body('newBandName');

    $tempSelect = "select album_name from albums where album_name = :album_name";
    $tempSelect_stt = $app_db_connection->prepare($tempSelect);
    $tempSelect_stt->execute([
        ':album_name' => $albumName
    ]);
    $tempResult = $tempSelect_stt->fetch(PDO::FETCH_OBJ);

    $update_band = "UPDATE albums SET album_name=:new_album_name,band_name= :new_band_name WHERE album_name = :album_name and band_name = :band_name";
    $update_band_stt = $app_db_connection->prepare($update_band);
    $update_band_stt->execute([
        ':album_name' => $albumName,
        ':band_name' => $bandName,
        ':new_album_name' => $newAlbumName,
        ':new_band_name' => $newBandName
    ]);


    $newSelect ="select album_name from albums where album_name = :album_name";
    $newSelect_stt = $app_db_connection->prepare($newSelect);
    $newSelect_stt->execute([
        ':album_name' => $albumName
    ]);
    $newResult = $newSelect_stt->fetch(PDO::FETCH_OBJ);

    if ($tempResult == $newResult) {
        $res->redirect("/add-album?update=0");
    } elseif ($tempResult != $newResult){$res->redirect("/add-album?update=1");}
}
?>