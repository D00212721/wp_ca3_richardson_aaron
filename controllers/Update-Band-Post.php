<?php

return function($req, $res) {


    $app_db_connection = database();


    $select = "select * from bands";
    $selectStatement = $app_db_connection->prepare($select);
    $selectStatement->execute();


    $bandName = $req->body('oldBandName');
    $bandYear = $req->body('oldBandYear');
    $newBandName = $req->body('newBandName');
    $newBandYear = $req->body('newBandYear');

    $tempSelect = "select band_name from bands where band_name = :band_name";
    $tempSelect_stt = $app_db_connection->prepare($tempSelect);
    $tempSelect_stt->execute([
        ':band_name' => $bandName
    ]);
    $tempResult = $tempSelect_stt->fetch(PDO::FETCH_OBJ);

    $update_band = "UPDATE bands SET band_name=:new_band_name,band_year= :new_band_year WHERE band_name = :band_name and band_year = :band_year";
    $update_band_stt = $app_db_connection->prepare($update_band);
    $update_band_stt->execute([
        ':band_name' => $bandName,
        ':band_year' => $bandYear,
        ':new_band_name' => $newBandName,
        ':new_band_year' => $newBandYear
    ]);


    $newSelect = "select band_name from bands where band_name = :band_name";
    $newSelect_stt = $app_db_connection->prepare($newSelect);
    $newSelect_stt->execute([
        ':band_name' => $bandName
    ]);
    $newResult = $newSelect_stt->fetch(PDO::FETCH_OBJ);


    if ($tempResult == $newResult) {
        $res->redirect("/add-band?update=0");
    } elseif ($tempResult != $newResult){$res->redirect("/add-band?update=1");}
    }
?>