<?php

  // Include the Rapid library
  require_once('lib/Rapid.php');

  // Create a new Router instance
  $app = new \Rapid\Router();

  $config = include_once ('config.php');
  define('rootPath',$config['rootPath']);
  $database = include_once ('database.php');




  // Define some routes. Here: requests to / will be
  // processed by the controller at controllers/Home.php
  $app->GET('/',        'Home');

  $app->GET('/add-album', 'Add-Album');
  $app->POST('/add-album','Add-Album-Post');
  $app->POST('/remove-album','Remove-Album-Post');
  $app->POST('/update-album','Update-Album-Post');

  $app->GET('/add-band', 'Add-Band');
  $app->POST('/add-band','Add-Band-Post');
  $app->POST('/remove-band','Remove-Band-Post');
  $app->POST('/update-band','Update-Band-Post');
  // Process the request
  $app->dispatch();

?>