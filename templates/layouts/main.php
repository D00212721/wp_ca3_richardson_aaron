<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title><?= $locals['pageTitle']?></title>
</head>
<body>

<h1>MUSIC SITE</h1>
<p>the place to go to look at song titles and not listen </p>

<nav>
    <ul>
        <li>
            <a href="/<?=rootPath?>/">Home</a>
        </li>
        <li>
            <a href="/<?=rootPath?>/add-band">Add Band</a>
        </li>
        <li>
            <a href="/<?=rootPath?>/add-album">Add Album</a>
        </li>
    </ul>

    <?= Rapid\Renderer::VIEW_PLACEHOLDER?>

</nav>


</body>
</html>
