<?php
$app_db_connection = database();
$query = "select band_name,band_year from bands";
$stt = $app_db_connection->prepare("$query");
$stt -> execute();
$count = $stt->rowCount();
?>


<h1>Add An Album To The Sites Table</h1>
<form action="add-album" method="post">
    <label for="AlbumName" id="AlbumNameLabel">Album Name:</label>
    <input  required="required" type="text" name="albumName" id="albumName">

    <label for="bandName" id="bandNameLabel">Band Album belongs to</label>
    <select name="bandName" id="bandName">
        <?php
        if($count >0)
        {
            $result = $stt->fetchAll(PDO::FETCH_OBJ);
            foreach($result as $row )
            {?>
                <option value="<?=$row->band_name?>"><?=$row->band_name?></option>
            <?php }?>
            <?php
            }?>
    </select>
    <input type="Submit" value="Submit">
</form>

<h1>Remove An Album From The Sites Table</h1>
<?php
$app_db_connection = database();
$query = "select band_name,band_year from bands";
$stt = $app_db_connection->prepare("$query");
$stt -> execute();
$count = $stt->rowCount();
?>


<form action="remove-album" method="post">
    <label for="albumName" id="albumNameLabel">Album Name:</label>
    <input  required="required" type="text" name="albumName" id="albumName">

    <label for="bandName" id="bandNameLabel">Band Album belongs to</label>
    <select name="bandName" id="bandName">
        <?php
        if($count >0)
        {
            $result = $stt->fetchAll(PDO::FETCH_OBJ);
            foreach($result as $row )
            {?>
                <option value="<?=$row->band_name?>"><?=$row->band_name?></option>
            <?php }?>
            <?php
        }?>
    </select>
    <input type="Submit" value="Delete">
</form>

<h1>Update An Album From The Sites Table</h1>
<?php
$app_db_connection = database();
$query = "select band_name,band_year from bands";
$stt = $app_db_connection->prepare("$query");
$stt -> execute();
$count = $stt->rowCount();
?>


<form action="update-album" method="post">
    <label for="oldAlbumName" id="oldAlbumNameLabel">Album Name:</label>
    <input  required="required" type="text" name="oldAlbumName" id="oldAlbumName">

    <label for="oldBandName" id="oldBandNameLabel">Band Album belongs to</label>
    <select name="oldBandName" id="oldBandName">
        <?php
        if($count >0)
        {
            $result = $stt->fetchAll(PDO::FETCH_OBJ);
            foreach($result as $row )
            {?>
                <option value="<?=$row->band_name?>"><?=$row->band_name?></option>
            <?php }?>
            <?php
        }?>
    </select>

    <?php
    $app_db_connection = database();
    $query = "select band_name,band_year from bands";
    $stt = $app_db_connection->prepare("$query");
    $stt -> execute();
    $count = $stt->rowCount();
    ?>

    <label for="newAlbumName" id="newAlbumNameLabel">NewAlbum Name:</label>
    <input  required="required" type="text" name="newAlbumName"id="newAlbumName">

    <label for="newBandName" id="newBandNameLabel">New Band Album belongs to</label>
    <select name="newBandName" id="newBandName">
        <?php
        if($count >0)
        {
            $result = $stt->fetchAll(PDO::FETCH_OBJ);
            foreach($result as $row )
            {?>
                <option value="<?=$row->band_name?>"><?=$row->band_name?></option>
            <?php }?>
            <?php
        }?>
    </select>

    <input type="Submit" value="Update">
</form>


<?php
if($locals['added'] === '1' )
{?>

    <h3>Success</h3>
    <p>added Album to data base</p>

<?php
}
if($locals['removed'] === '1' ) {
    ?>

    <h3>Success</h3>
    <p>removed Album from data base</p>
    <?php
}?>
<?php
if($locals['updated'] === '1' ) {
    ?>

    <h3>Success</h3>
    <p>updated Album in the data base</p>
    <?php
}?>

<?php
if ($locals['added']==='0'||$locals['removed']==='0'||$locals['updated']==='0'){?>

    <h3>Failure</h3>
    <p>something went wrong</p>

<?php
}
?>
<?php
$app_db_connection = database();
$query = "select album_name,band_name from albums";
$stt = $app_db_connection->prepare("$query");
$stt -> execute();
$count = $stt->rowCount();
?>



<?php
if($count >0)
{?>
<h3>Album Table</h3>
<table>
    <tr><th id = 'title'>Album</th><td>|-----|</td><th id = 'title'>Band</th></tr>

    <?php
    $result = $stt->fetchAll(PDO::FETCH_OBJ);
    foreach($result as $row )
    {?>
        <tr><td><?=$row->album_name?></td><td>|-----|</td><td><?=$row->band_name?></td></tr>
    <?php }?>
    <?php
    }?>
