
<h1>Add A Band To The Sites Table</h1>

<form action="add-band" method="post">
    <label for="bandName" id="bandNameLabel">Band Name:</label>
    <input type="text" required="required" id="bandName" name="bandName" placeholder="Name">
    <label for="bandYear" id="bandYearLabel">year formed:</label>
    <input type="number" required="required"  name="bandYear" id="bandYear" min="1900" max="2019" placeholder="2019">
    <input type="Submit" value="Submit">
</form>

<h1>Remove A Band From The Sites Table</h1>

<form action="remove-band" method="post">
    <label for="bandName" name="bandName"  id="bandNameLabel">Band Name:</label>
    <input type="text" required="required" name="bandName" id="bandName" placeholder="Name">
    <label for="bandYear" id="bandYearLabel">year formed:</label>
    <input type="number" required="required" name="bandYear"id="bandYear" min="1900" max="2019" placeholder="2019">
    <input type="Submit" value="Delete">
</form>

<h1>Update A Band From The Sites Table</h1>

<form action="update-band" method="post">
    <label for="oldBandName" id="oldBandNameLabel">Band Name:</label>
    <input type="text" required="required" id="oldBandName" name="oldBandName" placeholder="Name">
    <label for="oldBandYear" id="oldBandYearLabel">year formed:</label>
    <input type="number" required="required" name="oldBandYear" id="oldBandYear" min="1900" max="2019" placeholder="2019">

    <label for="newBandName" id="newBandNameLabel">New Band Name:</label>
    <input type="text" required="required" name="newBandName" id="newBandName" placeholder="Name">
    <label for="newBandYear" id="newBandYear">New Year:</label>
    <input type="number" name="newBandYear" required="required" id="bandYear" min="1900" max="2019" placeholder="2019">
    <input type="Submit" value="Update">
</form>



<?php

if($locals['added'] === '1' )
{?>

    <h3>Add Successful</h3>
    <p>added band to data base</p>

<?php
}
elseif($locals['added'] === '0')
{?>
    <h3>Failed</h3>
    <p>something went wrong sorry try again</p>
<?php
}
?>


<?php

if($locals['removed'] === '1' )
{?>

    <h3>Remove Successful</h3>
    <p>removed band from data base</p>

    <?php
}
elseif($locals['removed'] === '0')
{?>
    <h3>Failed</h3>
    <p>something went wrong sorry try again</p>
    <?php
}
?>


<?php

if($locals['updated'] === '1' )
{?>

    <h3>Update Successful</h3>
    <p>updated band in data base</p>

    <?php
}
elseif($locals['updated'] === '0')
{?>
    <h3>Failed</h3>
    <p>something went wrong sorry try again</p>
    <?php
}
?>
<?php
$app_db_connection = database();
$query = "select band_name,band_year from bands";
$stt = $app_db_connection->prepare("$query");
$stt -> execute();
$count = $stt->rowCount();
?>



<?php
if($count >0)
{?>
    <h3>Band Table</h3>
<table>
    <tr><th id = 'title'>Band</th><td>|-----|</td> <th id = 'title'>year formed</th></tr>

    <?php
    $result = $stt->fetchAll(PDO::FETCH_OBJ);
     foreach($result as $row )
     {?>
        <tr><td><?=$row->band_name?></td><td>|-----|</td><td><?=$row->band_year?></td></tr>
     <?php }?>
<?php
}?>